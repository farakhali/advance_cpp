#include <map>
#include <vector>
#include <iostream>

#include "student.h"
#include "whitelist.h"

using namespace emumba::training;

void whitelist::add_to_whitelist(const std::string &student_name, const student &student_data)
{
    _StudentDataList.push_front(student_data);                                                       // insert student record at the front/start of a list
    _StudentData.insert(std::pair<std::string, student *>(student_name, &_StudentDataList.front())); // returns reference to the first element in the list
}

bool whitelist::is_student_present(const std::string &student_name) const
{
    if (_StudentData.find(student_name) != _StudentData.end()) //  if found return iterator pointing to element otherwise nullptr
        return true;
    std::cout << "Student Not Present in the List: ";
    return false;
}

student *whitelist::get_student_data(const std::string &student_name) const
{
    auto it = _StudentData.find(student_name);
    if (it != _StudentData.end())
        return it->second;
    std::cout << "Student Not Present in the List\n";
    return nullptr;
}
