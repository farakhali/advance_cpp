#include <string>
#include <map>
#include <iostream>

#include "student.h"

using namespace emumba::training;

student::student()
{
    set_roll_no("NULL");
    set_age(0);
}
student::student(const std::string &roll_no, const int &age)
{
    set_roll_no(roll_no);
    set_age(age);
}

student::~student()
{
    // std::cout << get_roll_no() << "student() destructor called \n";
}

void student::print_data()
{
    std::cout << get_roll_no() << std::endl;
    std::cout << get_age() << std::endl;
}

int student::get_age() const
{
    return _StudentRecord._Age;
}
void student::set_age(const int &age)
{
    _StudentRecord._Age = age;
}

std::string student::get_roll_no() const
{
    return _StudentRecord._RollNumber;
}
void student::set_roll_no(const std::string &roll_no)
{
    _StudentRecord._RollNumber = roll_no;
}

void student::set_subj_mark(const std::string &subj,const int &mark)
{
    if (mark < 0 || mark > 100)
    {
        std::cout << "\n"
                  << subj << " Marks not set marks must be within 0 and 100.\n";
        return;
    }
    if (SubjectList.find(subj) != SubjectList.end())
    {
        std::cout << "\n"
                  << get_roll_no() << " " << subj << " Subject Mark Updated: ";
        SubjectList[subj] = mark;
    }
    else
    {
        std::cout << get_roll_no() << " " << subj << " Subject added";
        SubjectList.insert(std::pair<std::string, int>(subj, mark));
    }
    set_cgpa();
}
int student::get_subj_mark(const std::string &get_mark)
{
    if (SubjectList.find(get_mark) != SubjectList.end())
    {
        std::cout << get_mark << " Subject Marks : ";
        return SubjectList[get_mark];
    }
    else
    {
        std::cout << "\nSubject Not Found ";
        return -1;
    }
}

float student::get_cgpa()const
{
    return _StudentRecord._CGPA;
}
void student::set_cgpa()
{
    _StudentRecord._CGPA = 0;
    int sum = 0;
    int count = 0;
    for (std::map<std::string, int>::iterator itr = SubjectList.begin(); itr != SubjectList.end(); ++itr)
    {

        sum = sum + itr->second;
        count++;
    }
    _StudentRecord._CGPA = 4 * (sum / (count * 100));
}

void student::print_all_mark()
{
    bool flag = true; // bool
    for (std::map<std::string, int>::iterator itr = SubjectList.begin(); itr != SubjectList.end(); ++itr)
    {
        flag = false;
        std::cout << itr->first << " : " << itr->second << "\t";
    }
    if (flag)
    {
        std::cout << "No Subject Found\n";
    }
}