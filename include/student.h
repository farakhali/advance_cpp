#ifndef _STUDENT_H
#define _STUDENT_H

#include <iostream>
#include <string>
#include <map>

namespace emumba::training
{
    class student
    {
    private:
        struct student_record
        {
            std::string _RollNumber;
            int _Age;
            float _CGPA;
        } StudentRecord;
        std::map<std::string /*Subject  */, int /* Marks */> SubjectList;

    public:
        student();
        student(const std::string &roll_no, const int &age, const int &cgpa);

        ~student();

        void print_data();

        int get_age() const;
        void set_age(const int &age);

        std::string get_roll_no() const;
        void set_roll_no(const std::string &roll_no);

        float get_cgpa();
        void set_cgpa(float cgpa);

        void set_subj_mark(const std::string &subj, const int &mark);
        int get_subj_mark(const std::string &get_mark);

        void print_all_mark();
    };

}

#endif