#include <map>
#include <list>#include <vector>
#include "student.h"

namespace emumba::training
{
    class whitelist
    {
    private:
        std::map<std::string /* Student Name */, student * /* pointer to Student */> _StudentData;
        std::list<student> _StudentDataList;
        /* Reasson......
        -> Vector Container is just like Dynamic arrays are arrays whose size is determined while the program is
        running, rather than being fixed when the program is written. when ever its grows/increases its allocate
        new memory because of that reallocation the already stored pointer get NULL value and become useless.
        */
    public:  
        void add_to_whitelist(const std::string &student_name, const student &student_data);
        bool is_student_present(const std::string &student_name) const;
        student *get_student_data(const std::string &student_name) const;
    };
}
