#include <iostream>

#include "student.h"
#include "whitelist.h"

using namespace emumba::training;
void print_class(whitelist &mylist);
void get_sudent_record(whitelist &mylist);
int main()
{
    student *student_ptr;
    whitelist mylist;
    std::string name;
    
    student student_2("S2", 2);
    student student_3("S3", 3);
    student student_4("S4", 4);
    student student_5("S5", 5);
    student student_6("S6", 6);
    student student_7("S7", 7);
    student student_8("S8", 8);
    student student_9("S9", 9);
    student student_10("S10", 10);

    mylist.add_to_whitelist("student1", student("S1", 1));
    mylist.add_to_whitelist("student2", student_2);
    mylist.add_to_whitelist("student3", student_3);
    mylist.add_to_whitelist("student4", student_4);
    mylist.add_to_whitelist("student5", student_5);
    mylist.add_to_whitelist("student6", student_6);
    mylist.add_to_whitelist("student7", student_7);
    mylist.add_to_whitelist("student8", student_8);
    mylist.add_to_whitelist("student9", student_9);
    mylist.add_to_whitelist("student10", student_10);

    std::cout << "\n";

    print_class(mylist);

    std::cout<<mylist.is_student_present("student12") <<"\n";

    get_sudent_record(mylist);
    return 0;
}

void print_class(whitelist &mylist)
{
    std::string st_names[10] = {"student1", "student2", "student3", "student4", "student5", "student6", "student7", "student8", "student9", "student10"};
    for (int i = 0; i < 10; i++)
    {
        if (mylist.is_student_present(st_names[i]))
        {
            student *student_ptr = mylist.get_student_data(st_names[i]);
            std::cout << "Name : " << student_ptr->get_roll_no() << "\t\t"
                      << "Age : " << student_ptr->get_age() << "\n";
        }
    }
}

void get_sudent_record(whitelist &mylist)
{
    std::string name;
    std::cout<<"Enter Student Name to search : ";
    std::cin>>name;
    student *ptr;
    if (mylist.is_student_present(name))
    {
        ptr = mylist.get_student_data(name);
        ptr->set_subj_mark("eng", 12);
        ptr->set_subj_mark("phy", 13);
        std::cout << "\n\n\n\n ---------- Student Record -----------\n";
        ptr->set_cgpa();
        std::cout << "Name : " <<ptr->get_roll_no() << "\t\t"
                  << "Age : " << ptr->get_age() << "\t\t"
                  << "CGPA : " <<ptr->get_cgpa() << "\t\t"
                  << "\nSubjects Marks "
                  << "\n";
        ptr->print_all_mark();
        std::cout << "\n";
    }
}